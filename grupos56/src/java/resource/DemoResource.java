
package resource;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.UriInfo;
import javax.ws.rs.Produces;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PUT;
import javax.ws.rs.core.MediaType;
import model.Conexion;


@Path("demo")
public class DemoResource {



    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String saludar() {
        Conexion conex = new Conexion();
        conex.desconectar();
        return "hola grupo 56";
        
    }

}
